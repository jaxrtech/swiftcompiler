var people:int = 10;

func main() -> int {
    var result:int;
    result = calc(1, 2, 3);
    people = result;
    people *= 3;

    return people;
}

func calc(a:int, b:int, c:int) -> int {
    var result:int;
    result = 1;
    result += a;
    result *= b;
    result /= c;
    return result;
}
