package com.jaxrtech.swift;

import com.jaxrtech.swift.compiler.CompilationResult;
import com.jaxrtech.swift.compiler.CompileFlag;
import com.jaxrtech.swift.compiler.Compiler;

import java.io.FileNotFoundException;
import java.util.EnumSet;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        final long startTime = System.currentTimeMillis();

        String inputPath;
        String outputPath;

        if (args.length < 2) {
            printUsage();
            return;
        } else {
            inputPath = args[0];
            outputPath = args[1];
        }

        printAbout();

        EnumSet<CompileFlag> flags = EnumSet.of(CompileFlag.SHOW_AST);
        Compiler compiler = new Compiler(flags, inputPath, outputPath);
        CompilationResult result = compiler.run();

        final long endTime = System.currentTimeMillis();
        System.out.println("Completed in: " + ((endTime - startTime) / 1000.0) + " seconds");

        System.exit(result.getExitCode());
    }

    private static void printAbout() {
        System.out.println("Swift DCPU-16 Compiler");
        System.out.println("Copyright (c) 2013 Joshua Bowden / Jaxrtech");
    }

    private static void printUsage() {
        printAbout();
        System.out.println("usage: swift [input file] [output file]");
    }
}
