package com.jaxrtech.swift;

import com.jaxrtech.swift.generator.PrimitiveType;
import com.jaxrtech.swift.generator.entities.CompilableNode;
import com.jaxrtech.swift.entities.nodes.*;
import com.jaxrtech.swift.entities.nodes.expressions.ExpressionNode;
import com.jaxrtech.swift.entities.nodes.functions.FunctionCallNode;
import com.jaxrtech.swift.entities.nodes.functions.FunctionDeclarationNode;
import com.jaxrtech.swift.entities.nodes.parameters.ParameterCallNode;
import com.jaxrtech.swift.entities.nodes.parameters.ParameterCallSequenceNode;
import com.jaxrtech.swift.entities.nodes.parameters.ParameterDeclarationNode;
import com.jaxrtech.swift.entities.nodes.parameters.ParameterDeclarationSequenceNode;
import com.jaxrtech.swift.entities.operators.ArithmeticOperator;
import com.jaxrtech.swift.entities.operators.AssignmentOperator;
import org.parboiled.BaseParser;
import org.parboiled.Rule;
import org.parboiled.annotations.*;
import org.parboiled.support.Position;
import org.parboiled.support.Var;
import org.parboiled.trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

@BuildParseTree
public class Parser extends BaseParser<TreeNode> {
    /**
     * The starting rule for the entire parser.
     * @return the rule and pushes a {@link ProgramNode}
     */
    @SuppressWarnings("unchecked")
    public Rule Program() {
        List<TreeNode> nodes = new ArrayList<TreeNode>();

        return Sequence(
                Spacing(),
                ZeroOrMore(
                        GlobalDeclaration(), nodes.add(pop())
                ),
                EOI,

                push(new ProgramNode(nodes))
        );
    }

    Rule GlobalDeclaration() {
        return FirstOf(
                VariableDeclaration(),
                FunctionDeclaration()
        );
    }

    Rule FunctionDeclaration() {
        Var<IdentifierNode> name = new Var<IdentifierNode>();
        Var<ParameterDeclarationSequenceNode> parameters = new Var<ParameterDeclarationSequenceNode>();
        Var<IdentifierNode> returnType = new Var<IdentifierNode>();
        Var<StatementBlockNode> statements = new Var<StatementBlockNode>();

        return Sequence(
                FUNC,
                Identifier(), name.set((IdentifierNode) pop()),
                ParameterDeclarationSequence(), parameters.set((ParameterDeclarationSequenceNode) pop()),
                FunctionReturnType(), returnType.set((IdentifierNode) pop()),
                StatementBlock(), statements.set((StatementBlockNode) pop()),

                push(new FunctionDeclarationNode(name.get(), parameters.get(), returnType.get(), statements.get()))
        );
    }

    Rule FunctionReturnType() {
        return FirstOf(
                // Either actual specified
                Sequence(
                        FARROW, Identifier()
                ),
                // Or default to type of "void"
                push(new IdentifierNode("void"))
        );
    }

    @SuppressWarnings({"unchecked"})
    Rule ParameterDeclarationSequence() {
        ArrayList<ParameterDeclarationNode> parameters = new ArrayList<ParameterDeclarationNode>();

        return Sequence(
                LPAREN,
                Optional(
                        ParameterDeclaration(), parameters.add((ParameterDeclarationNode) pop()),
                        ZeroOrMore(Sequence(
                                COMMA,
                                ParameterDeclaration(), parameters.add((ParameterDeclarationNode) pop()))
                        )
                ),
                RPAREN,

                push(new ParameterDeclarationSequenceNode(parameters))
        );
    }

    Rule ParameterDeclaration() {
        Var<IdentifierNode> name = new Var<IdentifierNode>();
        Var<IdentifierNode> type = new Var<IdentifierNode>();

        return Sequence(
                Identifier(), name.set((IdentifierNode) pop()),
                COLON,
                BasicType(), type.set((IdentifierNode) pop()),

                push(new ParameterDeclarationNode(name.get(), type.get()))
        );
    }

    @SuppressWarnings({"unchecked"})
    Rule ParameterCallSequence() {
        List<ParameterCallNode> list = new ArrayList<ParameterCallNode>();

        return Sequence(
                LPAREN,
                Optional(
                        ParameterCall(), list.add((ParameterCallNode) pop()),
                        ZeroOrMore(
                                COMMA, ParameterCall(), list.add((ParameterCallNode) pop())
                        )
                ),
                RPAREN,
                push(new ParameterCallSequenceNode(list))
        );
    }

    Rule ParameterCall() {
        Var<IntegerLiteralNode> value = new Var<IntegerLiteralNode>();

        return Sequence(
                Integer(), value.set((IntegerLiteralNode) pop()),
                push(new ParameterCallNode(value.get()))
        );
    }

    @SuppressWarnings({"unchecked"})
    Rule StatementBlock() {
        List<CompilableNode> statements = new ArrayList<CompilableNode>();

        return Sequence(
                LCURLY,
                ZeroOrMore(Statement(), statements.add((CompilableNode) pop())),
                RCURLY,

                push(new StatementBlockNode(statements)),
                clearList(statements)
        );
    }

    /**
     * Helper function for clearing a list in rule
     * @param list the list to clear
     * @return true to continue the rule
     */
    boolean clearList(List list) {
        list.clear();
        return true;
    }

    Rule Statement() {
        return FirstOf(
                ReturnStatement(),
                VariableDeclaration(),
                AssignmentStatement(),
                FunctionCall()
        );
    }

    Rule FunctionCall() {
        Var<IdentifierNode> name = new Var<IdentifierNode>();
        Var<ParameterCallSequenceNode> parameters = new Var<ParameterCallSequenceNode>();

        return Sequence(
                Identifier(), name.set((IdentifierNode) pop()),
                ParameterCallSequence(), parameters.set((ParameterCallSequenceNode) pop()),
                SEMI,

                push(new FunctionCallNode(name.get(), parameters.get()))
        );
    }

    Rule ReturnStatement() {
        Var<ExpressionNode> value = new Var<ExpressionNode>();

        return Sequence(
                RETURN,
                Optional(AdditiveExpression(), value.set((ExpressionNode) pop())),
                SEMI,

                push(new ReturnStatementNode(value.get(), position()))
        );
    }

    Rule AssignmentStatement() {
        Var<IdentifierNode> name = new Var<IdentifierNode>();
        Var<AssignmentOperator> operator = new Var<AssignmentOperator>();
        Var<ExpressionNode> value = new Var<ExpressionNode>();

        return Sequence(
                Identifier(), name.set((IdentifierNode) pop()),
                AssignmentOperator(), operator.set(AssignmentOperator.fromString(match())),
                AdditiveExpression(), value.set((ExpressionNode) pop()),
                SEMI,

                push(new AssignmentStatementNode(name.get(), value.get(), operator.get()))
        );
    }

    Rule AssignmentOperator() {
        return FirstOf(
                EQUALS, PLUS_EQUALS, MINUS_EQUALS, TIMES_EQUALS, DIVIDE_EQUALS, MOD_EQUALS
        );
    }

    Rule VariableDeclaration() {
        Var<Position> position = new Var<Position>();
        Var<IdentifierNode> name = new Var<IdentifierNode>();
        Var<IdentifierNode> type = new Var<IdentifierNode>();
        Var<ExpressionNode> value = new Var<ExpressionNode>();
        Var<Boolean> isInitialized = new Var<Boolean>(true);

        return Sequence(
                position.set(position()),
                VAR,
                Identifier(), name.set((IdentifierNode) pop()),
                COLON,
                BasicType(), type.set((IdentifierNode) pop()),
                FirstOf(
                        Sequence(
                                // The variable has an initial value
                                EQUALS,
                                AdditiveExpression(), value.set((ExpressionNode) pop())
                        ),
                        Sequence(
                                // The variable is initialized with no value
                                value.set(new ExpressionNode(new IntegerLiteralNode((short) 0))),
                                isInitialized.set(false)
                        )
                ),
                SEMI,

                push(new VariableNode(name.get(), type.get(), value.get(), isInitialized.get(), position.get()))
        );
    }

    //---------------------------------------------------------------------------------
    //  Literals
    //---------------------------------------------------------------------------------

    /**
     * Helper for creating expression rules with a left expression, right expression, and an operator.
     * @param subRule the rule to use for the left and right side of the expression
     * @param operatorRule the rule for the operators to use
     * @return the rule and pushes an {@link ExpressionNode}
     */
    Rule ExpressionHelper(Rule subRule, Rule operatorRule) {
        Var<ArithmeticOperator> operator = new Var<ArithmeticOperator>();

        return Sequence(
                subRule,
                ZeroOrMore(
                        Sequence(operatorRule, !match().trim().equals(""), operator.set(ArithmeticOperator.fromString(match()))),
                        subRule,
                        push(new ExpressionNode(operator.get(), (ExpressionNode) pop(1), (ExpressionNode) pop()))
                )
        );
    }

    Rule AdditiveExpression() {
        return ExpressionHelper(MultiplicativeExpression(), FirstOf(PLUS, MINUS));
    }

    Rule MultiplicativeExpression() {
        return ExpressionHelper(PrimaryExpression(), FirstOf(TIMES, DIVIDE, MODULUS));
    }

    Rule PrimaryExpression() {
        return FirstOf(
                IntegerLiteral(),
                CharacterLiteral(),
                Sequence(
                        Identifier(), push(new ExpressionNode((IdentifierNode) pop()))
                ),
                ParenthesisExpression()
        );
    }

    Rule ParenthesisExpression() {
        return Sequence(
                LPAREN, AdditiveExpression(), RPAREN
        );
    }

    Rule IntegerLiteral() {
        return Sequence(
                IntegerValue(),
                push(new ExpressionNode((IntegerLiteralNode) pop()))
        );
    }

    Rule CharacterLiteral() {
        return Sequence(
                SINGLE_QUOTE,
                Character(),
                SINGLE_QUOTE,
                push(new ExpressionNode((CharacterLiteralNode) pop()))
        );
    }

    Rule Character() {
        return Sequence(
                CharacterRule(),
                push(new CharacterLiteralNode(matchedChar()))
        );
    }

    @DontLabel
    Rule CharacterRule() {
        return CharRange((char) 32, (char) 126); // Drawable ASCII range
    }

    Rule IntegerValue() {
        return FirstOf(
                HexInteger(),
                Integer()
        );
    }

    Rule Integer() {
        return Sequence(
                Sequence(
                        Optional(MINUS),
                        OneOrMore(Digit())
                ),
                push(new IntegerLiteralNode(Short.parseShort(matchOrDefault("0")))),
                Spacing()
        );
    }

    Rule HexInteger() {
        return Sequence(
                toRule("0x"),
                OneOrMore(Digit()),
                push(new IntegerLiteralNode(Short.parseShort(matchOrDefault("0"), 16))),
                Spacing()
        );
    }

    @DontLabel
    Rule Digit() {
        return CharRange('0', '9');
    }


    @MemoMismatches
    Rule BasicType() {
        return Sequence(
                FirstOf(
                        PrimitiveType.SIGNED_INT.toString(),
                        PrimitiveType.UNSIGNED_INT.toString(),
                        PrimitiveType.CHARACTER.toString()
                ),
                push(new IdentifierNode(match())),
                TestNot(LetterOrDigit()),
                Spacing()
        );
    }

    //---------------------------------------------------------------------------------
    //  Keywords
    //---------------------------------------------------------------------------------

    /**
     * Rule for a reserved language keyword.
     * @return the rule and does not push anything
     */
    @MemoMismatches
    Rule Keyword() {
        return Sequence(
                FirstOf("func", "return"),
                TestNot(LetterOrDigit())
        );
    }


    final Rule VAR = Keyword("var");
    final Rule FUNC = Keyword("func");
    final Rule RETURN = Keyword("return");

    /**
     * Helper rule for a keyword.
     * @param keyword the string value to match for the keyword
     * @return the rule for the keyword
     */
    @SuppressNode
    @DontLabel
    Rule Keyword(String keyword) {
        return Terminal(keyword, LetterOrDigit());
    }

    //---------------------------------------------------------------------------------
    //  Operators and Separators
    //---------------------------------------------------------------------------------

    // Symbols
    final Rule FARROW = Terminal("->");
    final Rule LPAREN = Terminal("(");
    final Rule RPAREN = Terminal(")");
    final Rule LCURLY = Terminal("{");
    final Rule RCURLY = Terminal("}");

    // Assignment Operators
    final Rule EQUALS = Terminal("=");
    final Rule PLUS_EQUALS = Terminal("+=");
    final Rule MINUS_EQUALS = Terminal("-=");
    final Rule TIMES_EQUALS = Terminal("*=");
    final Rule DIVIDE_EQUALS = Terminal("/=");
    final Rule MOD_EQUALS = Terminal("%=");

    // Arithmetic
    final Rule PLUS = Terminal("+");
    final Rule MINUS = Terminal("-");
    final Rule TIMES = Terminal("*");
    final Rule DIVIDE = Terminal("/");
    final Rule MODULUS = Terminal("%");

    // Separators
    final Rule COLON = Terminal(":");
    final Rule COMMA = Terminal(",");
    final Rule SEMI = Terminal(";");

    // Quotes
    final Rule SINGLE_QUOTE = Terminal("'");

    /**
     * Helper rule for a terminal that must not have any {@link #Spacing()} following.
     * @param string the string for the terminal
     * @return the terminal rule
     */
    @SuppressNode
    @DontLabel
    Rule Terminal(String string) {
        return Sequence(string, Spacing()).label('\'' + string + '\'');
    }

    /**
     * Helper rule for a terminal that must not have certain rule following.
     * @param string the string for the terminal
     * @param mustNotFollow the rule that must not following the terminal
     * @return the terminal rule
     */
    @SuppressNode
    @DontLabel
    Rule Terminal(String string, Rule mustNotFollow) {
        return Sequence(string, TestNot(mustNotFollow), Spacing()).label('\'' + string + '\'');
    }

    //---------------------------------------------------------------------------------
    //  Identifiers
    //---------------------------------------------------------------------------------

    /**
     * Rule for an identifier
     * @return the rule and pushes an {@link IntegerLiteralNode}
     */
    @SuppressSubnodes
    @MemoMismatches
    Rule Identifier() {
        return Sequence(
                TestNot(Keyword()),
                Sequence(Letter(), ZeroOrMore(LetterOrDigit())),
                push(new IdentifierNode(match())),
                Spacing());
    }

    /**
     * Rule for a valid letter
     * @return the rule
     */
    Rule Letter() {
        return FirstOf(CharRange('a', 'z'), CharRange('A', 'Z'), '_');
    }

    /**
     * Rule for a valid letter or digit
     * @return the rule
     */
    @MemoMismatches
    Rule LetterOrDigit() {
        return FirstOf(CharRange('a', 'z'), CharRange('A', 'Z'), CharRange('0', '9'), '_');
    }

    //---------------------------------------------------------------------------------
    //  Comments & Whitespace
    //---------------------------------------------------------------------------------

    /**
     * Rule for whitespace or comments
     * @return the rule
     */
    @SuppressNode
    @DontLabel
    Rule Spacing() {
        return ZeroOrMore(FirstOf(
                // whitespace
                OneOrMore(AnyOf(" \t\r\n\f").label("Whitespace")),

                // traditional comment
                Sequence("/*", ZeroOrMore(TestNot("*/"), ANY), "*/"),

                // end of line comment
                Sequence(
                        "//",
                        ZeroOrMore(TestNot(AnyOf("\r\n")), ANY),
                        FirstOf("\r\n", '\r', '\n', EOI)
                )
        ));
    }

    //---------------------------------------------------------------------------------
    //  Helpers
    //---------------------------------------------------------------------------------

    /**
     * Overrides the default parse tree creation to prevent creating node for single characters. This greatly increases
     * performance of the parser.
     * @param c the character to use as a rule
     * @return the rule
     */
    @Override
    protected Rule fromCharLiteral(char c) {
        // turn off creation of parse tree nodes for single characters
        return super.fromCharLiteral(c).suppressNode();
    }
}
