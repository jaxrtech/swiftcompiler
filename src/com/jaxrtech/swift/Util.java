package com.jaxrtech.swift;

import org.parboiled.trees.TreeNode;

import java.util.List;

public final class Util {
    public static boolean treeNodeHasAncestorOfType(TreeNode node, Class type) {
        // Keep getting the parent while its not the right type
        while (!type.isInstance(node)) {
            // We just ran up the tree with no more parents. This does not exist
            if (node.getParent() == null) {
                return false;
            }
            node = node.getParent();
        }
        // Found it
        return true;
    }

    public static boolean treeNodeHasChildOfType(TreeNode node, Class type) {
        if (node.getChildren().isEmpty()) {
            return false;
        }

        List children = node.getChildren();

        // Search each child
        for (Object child : children) {
            if (type.isInstance(child)) {
                return true;
            }

            // Search if its a sub-tree
            if (type.isInstance(TreeNode.class)) {
                boolean subtreeMatches = treeNodeHasChildOfType((TreeNode) child, type);
                if (subtreeMatches) {
                    return true;
                }
            }
        }

        return false;
    }

    public static TreeNode treeNodeGetFirstAncestorOfType(TreeNode node, Class type) throws IllegalStateException {
        // Keep getting the parent while its not the right type
        while (!type.isInstance(node)) {
            // We just ran up the tree with no more parents. This does not exist
            if (node.getParent() == null) {
                throw new IllegalStateException("No ancestor of the type specified was found");
            }
            node = node.getParent();
        }
        // Found it
        return node;
    }

    public static boolean listHasChildOfType(List list, Class type) {
        for (Object obj : list) {
            if (type.isInstance(obj)) return true;
        }
        return false;
    }
}
