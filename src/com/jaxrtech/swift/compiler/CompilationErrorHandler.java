package com.jaxrtech.swift.compiler;

public interface CompilationErrorHandler {
    void handle(CompilationException exception);
}
