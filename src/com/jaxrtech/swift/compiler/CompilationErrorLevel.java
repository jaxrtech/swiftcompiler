package com.jaxrtech.swift.compiler;

public enum CompilationErrorLevel {
    /**
     * Used to indicate a problem during the compilation process were the input was not valid, cannot be compiled,
     * and must have the compilation process halted immediately.
     */
    SEVERE(3, "Severe"),

    /**
     * Used to indicate a problem during the compilation process were the input was not valid and cannot be compiled.
     */
    ERROR(2, "Error"),

    /**
     * Used to indicate a problem during the compilation process were the input was not valid but still can be
     * compiled.
     */
    WARNING(1, "Warning"),

    /**
     * Used to indicate a notice or suggestion during the compilation process. This does not affect the compilation
     * result.
     */
    INFO(0, "Information");

    private int severity;
    private String name;

    private CompilationErrorLevel(int severity, String name) {
        this.severity = severity;
        this.name = name;
    }

    public int getSeverity() {
        return severity;
    }

    public String getName() {
        return name;
    }
}
