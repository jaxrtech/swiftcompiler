package com.jaxrtech.swift.compiler;

import org.parboiled.support.Position;

/**
 * Used to report a compiler exception encountered
 */
public class CompilationException extends Throwable {
    private CompilationErrorLevel level;
    private Position position;

    public CompilationException(CompilationErrorLevel level, String error) {
        super(error);
        this.level = level;
    }

    public CompilationException(CompilationErrorLevel level, Position position, String error) {
        super(error);
        this.level = level;
        this.position = position;
    }

    public CompilationErrorLevel getLevel() {
        return level;
    }

    public Position getPosition() {
        return position;
    }
}
