package com.jaxrtech.swift.compiler;

/**
 * Used to show how the compilation when.
 */
public enum CompilationResult {
    SUCCEEDED(0),
    FAILED(-1);

    private int exitCode;

    private CompilationResult(int exitCode) {
        this.exitCode = exitCode;
    }

    public int getExitCode() {
        return exitCode;
    }
}
