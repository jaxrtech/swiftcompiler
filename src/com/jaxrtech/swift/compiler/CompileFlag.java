package com.jaxrtech.swift.compiler;

public enum CompileFlag {
    SHOW_PARSE_TREE,
    SHOW_AST,
    DEBUG_MODE
}
