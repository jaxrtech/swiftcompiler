package com.jaxrtech.swift.compiler;

import com.jaxrtech.swift.Main;
import com.jaxrtech.swift.Parser;
import com.jaxrtech.swift.entities.nodes.ProgramNode;
import com.jaxrtech.swift.generator.Generator;
import org.parboiled.Parboiled;
import org.parboiled.parserunners.AbstractParseRunner;
import org.parboiled.parserunners.ReportingParseRunner;
import org.parboiled.parserunners.TracingParseRunner;
import org.parboiled.support.ParsingResult;
import org.parboiled.support.ToStringFormatter;
import org.parboiled.trees.GraphNode;

import java.io.*;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.logging.*;

import static org.parboiled.errors.ErrorUtils.printParseErrors;
import static org.parboiled.support.ParseTreeUtils.printNodeTree;
import static org.parboiled.trees.GraphUtils.printTree;

public class Compiler {
    public static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    private EnumSet<CompileFlag> flags = EnumSet.noneOf(CompileFlag.class);
    private String inputFilePath;
    private String outputFilePath;

    private CompilationResult currentStatus = CompilationResult.SUCCEEDED;
    private final List<CompilationException> errors = new ArrayList<CompilationException>();

    public Compiler(String inputFilePath, String outputFilePath) {
        this.inputFilePath = inputFilePath;
        this.outputFilePath = outputFilePath;
    }

    public Compiler(EnumSet<CompileFlag> flags, String inputFilePath, String outputFilePath) {
        this(inputFilePath, outputFilePath);
        this.flags = flags;
    }

    @SuppressWarnings({"unchecked"})
    public CompilationResult run() throws FileNotFoundException {
        setupLogger();

        // FIXME: putting the whole file im memory does not exactly work well with huge files
        // FIXME: there is probably a way to implement a InputBuffer using a stream for much better performance
        String code = getTextFromFile(inputFilePath);
        if (isInputForNullOrEmpty(code)) return CompilationResult.FAILED;

        Parser parser = Parboiled.createParser(Parser.class);
        AbstractParseRunner runner;
        ParsingResult<ProgramNode> result;

        runner = getParserRunnerFromFlags(parser);
        result = runner.run(code);

        if (flags.contains(CompileFlag.DEBUG_MODE) && runner instanceof TracingParseRunner) {
            System.out.println(((TracingParseRunner) runner).getLog());
        }

        if (result.hasErrors()) {
            reportCompileErrors(printParseErrors(result));
            return CompilationResult.FAILED;
        }

        Object root = result.parseTreeRoot.getValue();
        handleGenerationTimeFlags(result, (GraphNode) root);

        saveGeneratedToFile(outputFilePath, (ProgramNode) root, handler);
        reportAllCompilerErrors();
        handleCompilationResult();

        return currentStatus;
    }

    private void handleCompilationResult() {
        // Check if there were any errors above the ERROR level
        for (CompilationException exception : errors) {
            CompilationErrorLevel level = exception.getLevel();
            if (level.getSeverity() >= CompilationErrorLevel.ERROR.getSeverity()) {
                clearFile(outputFilePath);
                return;
            }
        }
    }

    private boolean isInputForNullOrEmpty(String code) {
        if (code == null || code.isEmpty()) {
            handler.handle(new CompilationException(CompilationErrorLevel.SEVERE, "Input was empty"));
            clearFile(outputFilePath);
            return true;
        }
        return false;
    }

    private CompilationErrorHandler handler = new CompilationErrorHandler() {
        @Override
        public void handle(CompilationException exception) {
            errors.add(exception);
        }
    };

    private String formatException(CompilationException exception) {
        if (exception.getPosition() == null) {
            return String.format("%s: %s%n",
                    exception.getLevel().getName(),
                    exception.getMessage());
        }

        return String.format("%s:(%d, %d) %s%n",
                exception.getLevel().getName(),
                exception.getPosition().line,
                exception.getPosition().column,
                exception.getMessage());
    }

    private void clearFile(String path) {
        try {
            PrintWriter writer = new PrintWriter(path);
            writer.print("");
            writer.close();
        } catch (FileNotFoundException ignored) {
        }
    }

    private static void saveGeneratedToFile(String path, ProgramNode root, CompilationErrorHandler errorHandler) {
        try {
            File file = new File(path);
            if (!file.exists()) {
                if (!file.createNewFile())
                    throw new IOException("Unable to create file");
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);

            Generator generator = new Generator(root);
            generator.generate(bw, errorHandler);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Unable to save output to file", ex);
        }
    }

    private void reportAllCompilerErrors() {
        StringBuilder builder = new StringBuilder();

        int counter = 0;
        for (CompilationException error : errors) {
            String formatted = formatException(error);
            String lineEnding = (counter == errors.size() - 1) ? "" : String.format("%n");

            builder.append(formatted);
            builder.append(lineEnding);

            counter++;
        }

        reportCompileErrors(builder.toString());
    }

    private void reportCompileErrors(String text) {
        System.err.println(text);
    }

    private AbstractParseRunner getParserRunnerFromFlags(Parser parser) {
        AbstractParseRunner runner;
        if (flags.contains(CompileFlag.DEBUG_MODE)) {
            runner = new TracingParseRunner(parser.Program());
        } else {
            runner = new ReportingParseRunner(parser.Program());
        }
        return runner;
    }

    @SuppressWarnings({"unchecked"})
    private void handleGenerationTimeFlags(ParsingResult<ProgramNode> result, GraphNode root) {
        if (flags.contains(CompileFlag.SHOW_AST)) {
            System.out.print(
                    "\nAbstract Syntax Tree:\n" +
                    printTree(root, new ToStringFormatter(null)) + '\n'
            );
        }
        if (flags.contains(CompileFlag.SHOW_PARSE_TREE)) {
            System.out.print(
                    "\nParse Tree:\n" + printNodeTree(result) + '\n');
        }
    }

    // FIXME: this is some really crappy copy-and-pasted code
    private static String getTextFromFile(String path) {
        StringBuilder code = new StringBuilder();
        BufferedReader reader = null;
        boolean error = false;

        try {
            File file = new File(path);
            reader = new BufferedReader(new FileReader(file));

            int c;
            while ((c = reader.read()) != -1) {
                char ch = (char) c;
                code.append(ch);
            }
        } catch (IOException ex) {
            errorFileOpen(path, ex);
            error = true;
        } finally {
            try {
                reader.close();
            } catch (Exception ex) {
                if (!error) errorFileOpen(path, ex);
            }
        }

        return code.toString();
    }

    private static void errorFileOpen(String path, Exception ex) {
        LOGGER.log(Level.SEVERE, "Unable to open source file at: '" + path + "'", ex);
    }

    private static void setupLogger() {
        LogManager.getLogManager().reset();

        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getLevel().getName() + ": " + record.getMessage();
            }
        });

        LOGGER.addHandler(handler);
    }
}
