package com.jaxrtech.swift.compiler;

import org.parboiled.buffers.InputBuffer;
import org.parboiled.support.IndexRange;
import org.parboiled.support.Position;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

/**
 * An attempt to get a stream to work an {@link InputBuffer} to help with performance and memeory concerns
 * NOTE: DO NOTE USE THIS!!! IT DOES NOT WORK (YET)!!!
 */
public class StreamedInputBuffer implements InputBuffer {
    private FileReader stream;

    public StreamedInputBuffer(String path) throws FileNotFoundException {
        this.stream = new FileReader(path);
    }

    @Override
    public char charAt(int index) {
        char[] buffer = new char[1];
        try {
            int read = stream.read(buffer, index, 1);
            if (read < 1) throw new IOException();

            return (char) buffer[0];
        } catch (IOException e) {
            return 0;
        }
    }

    @Override
    public boolean test(int index, char[] characters) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String extract(int start, int end) {
        final int count = end - start;
        char[] buffer = new char[count];

        try {
            int read = stream.read(buffer, start, count);
            if (read < 1) throw new IOException();
        } catch (IOException e) {
            return "";
        }

        return new String(buffer);
    }

    @Override
    public String extract(IndexRange range) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Position getPosition(int index) {
        return null;
    }

    @Override
    public int getOriginalIndex(int index) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String extractLine(int lineNumber) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getLineCount() {
        LineNumberReader lnr = new LineNumberReader(stream);
        try {
            lnr.skip(Long.MAX_VALUE);
        } catch (IOException e) {
            return -1;
        }

        return lnr.getLineNumber();
    }
}
