package com.jaxrtech.swift.entities.nodes;

import com.jaxrtech.swift.compiler.CompilationErrorHandler;
import com.jaxrtech.swift.generator.Context;
import com.jaxrtech.swift.generator.addresses.Addressable;
import com.jaxrtech.swift.generator.entities.*;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;
import com.jaxrtech.swift.generator.instructions.BasicInstruction;
import com.jaxrtech.swift.generator.opcodes.BasicOpcode;
import com.jaxrtech.swift.entities.nodes.expressions.ExpressionNode;
import com.jaxrtech.swift.entities.nodes.expressions.ExpressionType;
import com.jaxrtech.swift.entities.operators.AssignmentOperator;
import org.parboiled.trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class AssignmentStatementNode<T extends TreeNode<T>> extends CompilableNode<T> {
    private IdentifierNode name;
    private AssignmentOperator operator;
    private ExpressionNode value;

    public AssignmentStatementNode(IdentifierNode name, ExpressionNode value, AssignmentOperator operator) {
        this.name = name;
        this.operator = operator;
        this.value = value;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<T> getChildren() {
        List<T> list = new ArrayList<T>();
        list.add((T) name);
        list.add((T) value);

        return list;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<AssemblyLine> generate(Context context, CompilationErrorHandler errorHandler) {
        String relativeName = name.getValue();
        List<AssemblyLine> lines = new ArrayList<AssemblyLine>();
        switch (context.getTypeFromRelativeName(relativeName)) {
            case GLOBAL_VARIABLE:
                BasicOpcode opcode = BasicOpcode.fromAssignmentOperator(operator);
                if (opcode == null) { throw new IllegalStateException("Invalid assignment operator"); }
                Addressable address = context.getAddressFromRelative(relativeName);
                if (value.isConstantValue()) {
                    short assignedValue = value.evaluateConstant();
                    lines.add(new BasicInstruction(opcode, address, assignedValue));
                } else if (value.getType() == ExpressionType.IDENTIFIER) {
                    Addressable valueAddress = context.getAddressFromRelative(value.getIdentifier().getValue());
                    lines.add(new BasicInstruction(opcode, address, valueAddress));
                } else {
                    // TODO: implement OPERATOR_PAIR stuff
                    throw new UnsupportedOperationException("Cannot assign to that type of value yet");
                }
                break;
            case LOCAL_VARIABLE:
                Addressable localVarAddress = context.getAddressFromRelative(relativeName);
                BasicOpcode localVarOpcode = BasicOpcode.fromAssignmentOperator(operator);

                if (localVarOpcode == null) {
                    throw new IllegalStateException("Invalid assignment operator");
                }

                if (value.isConstantValue()) {
                    short assignedValue = value.evaluateConstant();
                    lines.add(new BasicInstruction(localVarOpcode, localVarAddress, assignedValue));
                } else {
                    lines.addAll(value.generateEvaluationTo(localVarAddress, context));
                }
                break;
            case INVALID:
                throw new IllegalStateException("Invalid relative reference name of '" + relativeName + "'");
            case FUNCTION:
                break;
            default:
                throw new UnsupportedOperationException("Only can assign to variables");
        }

        return lines;
    }

    @Override
    public String toString() {
        return "Assignment Statement | operator: " + operator.toString();
    }
}
