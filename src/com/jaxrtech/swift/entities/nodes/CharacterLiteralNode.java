package com.jaxrtech.swift.entities.nodes;

import com.jaxrtech.swift.entities.nodes.expressions.EvaluableToWord;
import org.parboiled.trees.ImmutableTreeNode;

public class CharacterLiteralNode extends ImmutableTreeNode<CharacterLiteralNode> implements EvaluableToWord {
    private char value;

    public CharacterLiteralNode(char c) {
        this.value = c;
    }

    @Override
    public short getValue() {
        return (short) value;
    }

    @Override
    public String toString() {
        return "[Char] " + value;
    }
}
