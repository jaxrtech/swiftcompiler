package com.jaxrtech.swift.entities.nodes;

import org.parboiled.trees.ImmutableTreeNode;
import org.parboiled.trees.TreeNode;

public class IdentifierNode <T extends TreeNode<T>> extends ImmutableTreeNode<T> {
    private String value;
    private String id;

    public IdentifierNode(String value) {
        this.value = value;
    }

    public IdentifierNode(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        String result = "";
        if (id != null) result += id + " = ";
        result += "Identifier | value: " + value;

        return result;
    }
}
