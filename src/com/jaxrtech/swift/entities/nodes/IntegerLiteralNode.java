package com.jaxrtech.swift.entities.nodes;

import com.jaxrtech.swift.entities.nodes.expressions.EvaluableToWord;
import org.parboiled.trees.ImmutableTreeNode;

public class IntegerLiteralNode extends ImmutableTreeNode<IntegerLiteralNode> implements EvaluableToWord {
    // Remember that an integer in the language is a 16-bit signed integer
    private short value;

    public IntegerLiteralNode(short value) {
        this.value = value;
    }

    @Override
    public short getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "[Integer] " + value;
    }
}
