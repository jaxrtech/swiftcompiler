package com.jaxrtech.swift.entities.nodes;

import com.jaxrtech.swift.compiler.CompilationErrorHandler;
import com.jaxrtech.swift.compiler.CompilationException;
import com.jaxrtech.swift.generator.Context;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;
import com.jaxrtech.swift.generator.entities.CompilableNode;
import org.parboiled.trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class ProgramNode<T extends TreeNode<T>> extends CompilableNode<T> {
    private List<CompilableNode> nodes = new ArrayList<CompilableNode>();

    public ProgramNode(List<CompilableNode> nodes) {
        this.nodes.addAll(nodes);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> getChildren() {
        List<T> list = new ArrayList<T>();
        for (TreeNode node : nodes)
            list.add((T) node);

        return list;
    }

    @Override
    public List<AssemblyLine> generate(Context context, CompilationErrorHandler errorHandler) {
        Context currentContext = Context.fromPrevious(this, context);

        List<CompilationException> exceptions = new ArrayList<CompilationException>();
        List<AssemblyLine> lines = new ArrayList<AssemblyLine>();
        for (CompilableNode node : nodes) {
            int index;
            if (!(node instanceof VariableNode)) {
                // Make sure to add all other nodes before the global variables
                index = 0;
            } else {
                index = lines.size();
            }

            lines.addAll(index, node.generate(currentContext, errorHandler));
        }

        return lines;
    }

    @Override
    public String toString() {
        return "Program |";
    }
}
