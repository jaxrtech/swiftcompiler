package com.jaxrtech.swift.entities.nodes;

import com.jaxrtech.swift.compiler.CompilationErrorHandler;
import com.jaxrtech.swift.compiler.CompilationErrorLevel;
import com.jaxrtech.swift.compiler.CompilationException;
import com.jaxrtech.swift.generator.Context;
import com.jaxrtech.swift.generator.addresses.Register;
import com.jaxrtech.swift.generator.entities.CompilableNode;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;
import com.jaxrtech.swift.entities.nodes.expressions.ExpressionNode;
import com.jaxrtech.swift.entities.nodes.functions.FunctionDeclarationNode;
import com.jaxrtech.swift.generator.instructions.EmptyAssemblyLine;
import org.parboiled.support.Position;
import org.parboiled.trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class ReturnStatementNode<T extends TreeNode<T>> extends CompilableNode<T> {
    // TODO: might want to move this to the calling convention
    private static final Register RETURN_REGISTER = Register.A;
    private Position position;
    private ExpressionNode value;

    public ReturnStatementNode(ExpressionNode value, Position position) {
        this.value = value;
        this.position = position;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<T> getChildren() {
        List<T> list = new ArrayList<T>();
        list.add((T) value);
        return list;
    }

    /*
     * TODO: the return statement should manage where to do the "set pc, pop" and exit the stack frame in the context
     * TODO: so that when you mode a return statement before a line, the function will actually return first and not
     * TODO: run it
     */
    @Override
    @SuppressWarnings({"unchecked"})
    public List<AssemblyLine> generate(Context context, CompilationErrorHandler errorHandler) {
        Context currentContext = Context.fromPrevious(this, context);

        // Make sure up the tree we are in a function definition
        if (!currentContext.hasAncestorOfType(FunctionDeclarationNode.class)) {
            errorHandler.handle(
                    new CompilationException(CompilationErrorLevel.ERROR, position,
                        "Return statement is not inside of a function definition."));
            return EmptyAssemblyLine.getInstanceAsList();
        }

        ArrayList<AssemblyLine> lines = new ArrayList<AssemblyLine>();

        // Put the return value in the right spot
        if (value != null) {
            lines.addAll(value.generateEvaluationTo(RETURN_REGISTER, currentContext));
        }

        // We need to clean up and exit the stack frame for the function
        FunctionDeclarationNode functionDefinition = (FunctionDeclarationNode) currentContext.getFirstAncestorOfType(FunctionDeclarationNode.class);
        lines.addAll(functionDefinition.exitStackFrame(context));

        return lines;
    }

    @Override
    public String toString() {
        return "Return Statement |";
    }
}
