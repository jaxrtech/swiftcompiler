package com.jaxrtech.swift.entities.nodes;

import com.jaxrtech.swift.compiler.CompilationErrorHandler;
import com.jaxrtech.swift.generator.Context;
import com.jaxrtech.swift.generator.entities.CompilableNode;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;
import org.parboiled.trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class StatementBlockNode<T extends TreeNode<T>> extends CompilableNode<T> {
    private List<CompilableNode> statements = new ArrayList<CompilableNode>();

    public StatementBlockNode(List<CompilableNode> statements) {
        List<CompilableNode> list = new ArrayList<CompilableNode>();
        for (CompilableNode node : statements) {
            list.add(node);
        }
        this.statements = list;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<T> getChildren() {
        List<T> list = new ArrayList<T>();
        for (TreeNode node : statements)
            list.add((T) node);

        return list;
    }

    @Override
    public List<AssemblyLine> generate(Context context, CompilationErrorHandler errorHandler) {
        Context currentContext = Context.fromPrevious(this, context);

        List<AssemblyLine> lines = new ArrayList<AssemblyLine>();
        for (CompilableNode node : statements) {
            lines.addAll(node.generate(currentContext, errorHandler));
        }

        return lines;
    }

    @Override
    public String toString() {
        return "Statement Block | ";
    }
}
