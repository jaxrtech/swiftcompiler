package com.jaxrtech.swift.entities.nodes;

import com.jaxrtech.swift.compiler.CompilationErrorHandler;
import com.jaxrtech.swift.compiler.CompilationErrorLevel;
import com.jaxrtech.swift.compiler.CompilationException;
import com.jaxrtech.swift.generator.Context;
import com.jaxrtech.swift.generator.GlobalType;
import com.jaxrtech.swift.generator.PrimitiveType;
import com.jaxrtech.swift.generator.addresses.QualifiedName;
import com.jaxrtech.swift.generator.entities.CompilableNode;
import com.jaxrtech.swift.generator.entities.LocalVariable;
import com.jaxrtech.swift.generator.instructions.ArrayInstruction;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;
import com.jaxrtech.swift.generator.instructions.EmptyAssemblyLine;
import com.jaxrtech.swift.generator.instructions.Label;
import com.jaxrtech.swift.generator.opcodes.ArrayOpcode;
import com.jaxrtech.swift.entities.nodes.expressions.ExpressionNode;
import org.parboiled.support.Position;
import org.parboiled.trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class VariableNode<T extends TreeNode<T>> extends CompilableNode<T> implements QualifiedName {
    // TODO: We are keeping the variable simple to get it work first
    private IdentifierNode name;
    private IdentifierNode type;
    private ExpressionNode value;
    private boolean isInitialized;

    public VariableNode(IdentifierNode name, IdentifierNode type, ExpressionNode value, boolean isInitialized, Position position) {
        this.name = new IdentifierNode("name", name.getValue());
        this.type = new IdentifierNode("type", type.getValue());
        this.value = value;
        this.isInitialized = isInitialized;
        this.position = position;
    }

    public ExpressionNode getValue() {
        return value;
    }

    public IdentifierNode getName() {
        return name;
    }

    public int getWordSize() {
        PrimitiveType primitive = PrimitiveType.fromName(type.getValue());
        if (primitive == null) throw new UnsupportedOperationException("Non-primitive types are not supported");
        return primitive.getWordSize();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> getChildren() {
        ArrayList<T> list = new ArrayList<T>();
        list.add((T) name);
        list.add((T) type);
        list.add((T) value);

        return list;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<AssemblyLine> generate(Context context, CompilationErrorHandler errorHandler) {
        Context currentContext = Context.fromPrevious(this, context);

        ArrayList<AssemblyLine> lines = new ArrayList<AssemblyLine>();

        TreeNode parent = currentContext.getParentNode();
        if (parent instanceof ProgramNode) {
            // Global variable
            context.setGlobalVariable(this);
            lines.add(new Label(getQualifiedName()));

            short[] dataValue;
            if (!isInitialized) {
                dataValue = new short[] { 0 };
            } else if (value.isConstantValue()) {
                dataValue = new short[] { value.evaluateConstant() };
            } else {
                errorHandler.handle(
                    new CompilationException(CompilationErrorLevel.ERROR, position,
                            "Cannot initialize a global variable with a value that is not constant."));
                return EmptyAssemblyLine.getInstanceAsList();
            }
            lines.add(new ArrayInstruction(ArrayOpcode.DAT, dataValue));
        } else if (parent instanceof StatementBlockNode) {
            if (isInitialized) {
                // Need to first get the local variable from the context so that we have the stack offset
                // All the stack variables were already added at start of the function declaration
                String varName = name.getValue();
                String qualifiedName = context.getQualifiedFromRelative(varName);
                LocalVariable variable = context.getLocalVariable(qualifiedName);
                lines.addAll(value.generateEvaluationTo(variable, context));
            }
        } else {
            throw new IllegalStateException("Invalid variable generation state");
        }

        return lines;
    }

    @Override
    public String toString() {
        return "Variable Declaration |";
    }

    @Override
    public String getGlobalPrefix() {
        return GlobalType.GLOBAL_VARIABLE.getGlobalPrefix();
    }

    @Override
    public String getQualifiedName() {
        return getGlobalPrefix() + name.getValue();
    }
}
