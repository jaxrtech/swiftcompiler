package com.jaxrtech.swift.entities.nodes.expressions;

public interface EvaluableToWord {
    short getValue();
}
