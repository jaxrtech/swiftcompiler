package com.jaxrtech.swift.entities.nodes.expressions;

import com.jaxrtech.swift.entities.nodes.IdentifierNode;
import com.jaxrtech.swift.entities.operators.ArithmeticOperator;
import com.jaxrtech.swift.generator.Context;
import com.jaxrtech.swift.generator.addresses.Addressable;
import com.jaxrtech.swift.generator.addresses.Register;
import com.jaxrtech.swift.generator.addresses.RegisterType;
import com.jaxrtech.swift.generator.addresses.RegisterWrapper;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;
import com.jaxrtech.swift.generator.instructions.AssemblyListingWithRegister;
import com.jaxrtech.swift.generator.instructions.BasicInstruction;
import com.jaxrtech.swift.generator.opcodes.BasicOpcode;
import org.parboiled.trees.BinaryTreeNode;
import org.parboiled.trees.ImmutableBinaryTreeNode;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ExpressionNode<T extends BinaryTreeNode<T>> extends ImmutableBinaryTreeNode<T> implements Cloneable {
    private ExpressionType type;
    private EvaluableToWord value;
    private IdentifierNode identifier;
    private ArithmeticOperator operator;

    @SuppressWarnings({"unchecked"})
    public ExpressionNode(EvaluableToWord value) {
        super(null, null);
        this.value = value;
        this.type = ExpressionType.VALUE;
    }

    public ExpressionNode(IdentifierNode identifier) {
        super(null, null);
        this.identifier = identifier;
        this.type = ExpressionType.IDENTIFIER;
    }

    @SuppressWarnings("unchecked")
    public ExpressionNode(ArithmeticOperator operator, ExpressionNode left, ExpressionNode right) {
        super((T) left, (T) right);
        this.operator = operator;
        this.type = ExpressionType.OPERATOR_PAIR;
    }

    public ExpressionType getType() {
        return type;
    }

    public EvaluableToWord getValue() {
        return value;
    }

    public IdentifierNode getIdentifier() {
        return identifier;
    }

    public ArithmeticOperator getOperator() {
        return operator;
    }

    public boolean isConstantValue() {
        switch (type) {
            case VALUE:
                return true; // this is a number
            case IDENTIFIER:
                return false;
            default:
                // this must still be an expression so keep going down the tree
                return ((ExpressionNode) left()).isConstantValue() && ((ExpressionNode) right()).isConstantValue();
        }
    }

    // TODO: this is going to be smarter especially with integer division and decimals and such
    public short evaluateConstant() {
        if (!isConstantValue()) throw new IllegalStateException("Cannot cannot evaluate a non-constant value as a constant");
        if (type == ExpressionType.VALUE) return value.getValue();

        short left = ((ExpressionNode) left()).evaluateConstant();
        short right = ((ExpressionNode) right()).evaluateConstant();
        switch (operator) {
            case ADDITION:
                return (short)(left + right);
            case SUBTRACTION:
                return (short)(left - right);
            case MULTIPLICATION:
                return (short)(left * right);
            case DIVISION:
                if (right == 0) {
                    System.err.println("Error: Cannot divide by zero");
                }
                return (short)(left / right);
            case MODULUS:
                return (short)(left % right);
            default:
                throw new IllegalStateException("Invalid operator");
        }
    }

    public List<AssemblyLine> generateEvaluationTo(final Addressable address, Context context) {
        return generateEvaluationTo(address, context, null);
    }

    @SuppressWarnings({"unchecked"})
    private List<AssemblyLine> generateEvaluationTo(final Addressable address, final Context context, final ArithmeticOperator operator) {
        ArrayList<AssemblyLine> lines = new ArrayList<AssemblyLine>();

        // Check if there is only one item in the tree that can be evaluated
        if (type == ExpressionType.IDENTIFIER || type == ExpressionType.VALUE) {
            if (operator == null) {
                // Must be setting to the address since there is no operator
                switch (type) {
                    case VALUE:
                        lines.add(BasicInstruction.set(address, value.getValue()));
                        return lines;
                    case IDENTIFIER:
                        Addressable var = context.getAddressFromRelative(identifier.getValue());
                        lines.add(BasicInstruction.set(address, var));
                        return lines;
                }
            } else {
                // Otherwise apply the value to the address with the opcode
                BasicOpcode opcode = BasicOpcode.fromArithmeticOperator(operator);
                switch (type) {
                    case VALUE:
                        lines.add(new BasicInstruction(opcode, address, value.getValue()));
                        return lines;
                    case IDENTIFIER:
                        Addressable var = context.getAddressFromRelative(identifier.getValue());
                        lines.add(new BasicInstruction(opcode, address, var));
                        return lines;
                }
            }
        }

        // Need a copy of this too for the inner function
        final ExpressionNode node = cloneHelper(this);

        // We must be dealing with an expression pair since it must not be a value or identifier
        AssemblyListingWithRegister listing = context.getRegisterBank().autoManageNextRegister(RegisterType.CALLEE_USE, new RegisterWrapper() {
            @Override
            public List<AssemblyLine> generate(Register register) {
                ArrayList<AssemblyLine> lines = new ArrayList<AssemblyLine>();

                // Get the expression that we just saved to use it
                ExpressionNode currentNode = node;

                // Get the bottom left leaf of the expression
                while (currentNode.left() != null && currentNode.right() != null) {
                    currentNode = (ExpressionNode) currentNode.left();
                }

                // Set the register to the leaf by running this again
                lines.addAll(currentNode.generateEvaluationTo(register, context, null));

                // Start going up from the leaf
                while (currentNode.getParent() != null) {
                    ExpressionNode parent = (ExpressionNode) currentNode.getParent();
                    ExpressionNode right = (ExpressionNode) parent.right();
                    // Remove the right item's parent so that it does not try to evaluate a level higher than itself
                    removeParent(right);
                    // Get the right item and apply it the register
                    lines.addAll(right.generateEvaluationTo(register, context, parent.getOperator()));

                    // Advance up the tree
                    currentNode = parent;
                }

                return lines;
            }
        });
        lines.addAll(listing.getLines());

        if (operator == null) {
            // Set the value back to the address
            lines.add(BasicInstruction.set(address, listing.getRegister()));
        } else {
            // Apple the value back to the address
            BasicOpcode opcode = BasicOpcode.fromArithmeticOperator(operator);
            lines.add(new BasicInstruction(opcode, address, listing.getRegister()));
        }

        return lines;
    }

    private static void removeParent(ExpressionNode node) {
        try {
            // Get the private field
            final Field field = node.getClass().getSuperclass().getSuperclass().getDeclaredField("parent"); // yay inheritance
            // Allow modification on the field
            field.setAccessible(true);
            // Sets the field to the new value for this instance
            field.set(node, null);
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException("Cannot get the field for the parent of the node");
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("Cannot get the field for the parent of the node");
        }
    }

    private static ExpressionNode cloneHelper(ExpressionNode node) {
        try {
            return (ExpressionNode) node.clone();
        } catch (CloneNotSupportedException e) {
            throw new IllegalStateException("Cannot clone the expression");
        }
    }

    @Override
    public String toString() {
        String result;
        if (isConstantValue()) {
            result = "Expression = " + evaluateConstant() + " |";
        } else {
            result = "Expression |";
        }

        if (value != null) {
            result += " value = " + value.toString();
        } else if (operator != null) {
            result += " operator = " + operator.toString();
        } else if (identifier != null) {
            result += " identifier = " + identifier.getValue();
        }

        return result;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
