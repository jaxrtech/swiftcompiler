package com.jaxrtech.swift.entities.nodes.expressions;

public enum ExpressionType {
    VALUE,
    IDENTIFIER,
    OPERATOR_PAIR
}
