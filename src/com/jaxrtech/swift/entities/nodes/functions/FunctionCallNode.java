package com.jaxrtech.swift.entities.nodes.functions;

import com.jaxrtech.swift.compiler.CompilationErrorHandler;
import com.jaxrtech.swift.generator.Context;
import com.jaxrtech.swift.generator.GlobalType;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;
import com.jaxrtech.swift.generator.entities.CallingConvention;
import com.jaxrtech.swift.generator.entities.CompilableNode;
import com.jaxrtech.swift.generator.entities.FastCallConvention;
import com.jaxrtech.swift.entities.nodes.IdentifierNode;
import com.jaxrtech.swift.entities.nodes.parameters.ParameterCallNode;
import com.jaxrtech.swift.entities.nodes.parameters.ParameterCallSequenceNode;
import org.parboiled.trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class FunctionCallNode<T extends TreeNode<T>> extends CompilableNode<T> {
    public static final CallingConvention DEFAULT_CALLING_CONVENTION = new FastCallConvention();

    private IdentifierNode name;
    private ParameterCallSequenceNode parameters;

    public FunctionCallNode(IdentifierNode name, ParameterCallSequenceNode parameters) {
        this.name = name;
        this.parameters = parameters;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<AssemblyLine> generate(Context context, CompilationErrorHandler errorHandler) {
        Context currentContext = Context.fromPrevious(this, context);

        GlobalType type = currentContext.getTypeFromRelativeName(name.getValue());
        if (type == GlobalType.INVALID) {
            throw new IllegalStateException("The name '" + name.getValue() + "' is invalid in the current context.");
        } else if (type != GlobalType.FUNCTION) {
            throw new IllegalStateException("Cannot call something that is not a function.");
        }

        String qualifiedName = currentContext.getQualifiedFromRelative(name.getValue());
        // TODO: right now we are assuming we are just passing a bunch of words around
        // Get copy the parameter values
        List<ParameterCallNode> params = parameters.getParameters();
        short[] values = new short[params.size()];
        for (int i = 0; i < params.size(); i++) {
            values[i] = params.get(i).getValue().getValue();
        }

        return DEFAULT_CALLING_CONVENTION.call(qualifiedName, values);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<T> getChildren() {
        List<T> list = new ArrayList<T>();
        list.add((T) name);
        list.add((T) parameters);
        return list;
    }

    @Override
    public String toString() {
        return "Function Call |";
    }
}
