package com.jaxrtech.swift.entities.nodes.functions;

import com.jaxrtech.swift.Util;
import com.jaxrtech.swift.compiler.CompilationErrorHandler;
import com.jaxrtech.swift.compiler.CompilationErrorLevel;
import com.jaxrtech.swift.compiler.CompilationException;
import com.jaxrtech.swift.entities.nodes.ReturnStatementNode;
import com.jaxrtech.swift.generator.Context;
import com.jaxrtech.swift.generator.GlobalType;
import com.jaxrtech.swift.generator.entities.LocalVariable;
import com.jaxrtech.swift.generator.addresses.QualifiedName;
import com.jaxrtech.swift.generator.addresses.Register;
import com.jaxrtech.swift.generator.addresses.StackLocation;
import com.jaxrtech.swift.generator.entities.CompilableNode;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;
import com.jaxrtech.swift.generator.instructions.BasicInstruction;
import com.jaxrtech.swift.generator.instructions.Label;
import com.jaxrtech.swift.generator.opcodes.BasicOpcode;
import com.jaxrtech.swift.entities.nodes.IdentifierNode;
import com.jaxrtech.swift.entities.nodes.StatementBlockNode;
import com.jaxrtech.swift.entities.nodes.VariableNode;
import com.jaxrtech.swift.entities.nodes.parameters.ParameterDeclarationSequenceNode;
import org.parboiled.trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class FunctionDeclarationNode<T extends TreeNode<T>> extends CompilableNode<T> implements QualifiedName {
    public final static Register BASE_STACK_POINTER_REGISTER = Register.J;

    private IdentifierNode identifier;
    private ParameterDeclarationSequenceNode parameters;
    private IdentifierNode returnType;
    private StatementBlockNode statements;

    private int localSize;

    public FunctionDeclarationNode(IdentifierNode identifier, ParameterDeclarationSequenceNode parameters, IdentifierNode returnType,
                                   StatementBlockNode statements) {
        this.identifier = new IdentifierNode("name", identifier.getValue());
        this.parameters = parameters;
        this.returnType = new IdentifierNode("returnType", returnType.getValue());
        this.statements = statements;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> getChildren() {
        ArrayList<T> list = new ArrayList<T>();
        list.add((T) identifier);
        list.add((T) parameters);
        list.add((T) returnType);
        list.add((T) statements);

        return list;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<AssemblyLine> generate(Context context, CompilationErrorHandler errorHandler) {
        Context currentContext = Context.fromPrevious(this, context);
        context.setGlobalFunction(this);

        ArrayList<AssemblyLine> lines = new ArrayList<AssemblyLine>();
        lines.add(new Label(getQualifiedName()));

        // Enter stack frame
        // Get stack space needed from variables
        for (Object statement : statements.getChildren()) {
            if (!(statement instanceof VariableNode)) { continue; }
            VariableNode variable = (VariableNode) statement;
            // Set the local variable at the current location on the stack
            context.setLocalVariable(new LocalVariable(variable, localSize));
            localSize += variable.getWordSize();
        }

        lines.addAll(setupStackFrame(context));

        // Statements
        lines.addAll(statements.generate(currentContext, errorHandler));

        // Exit stack frame
        // The return call should clean up the stack frame and return

        // Check for a return statement in the function
        boolean hasReturn = Util.treeNodeHasChildOfType(statements, ReturnStatementNode.class);
        if (!hasReturn) {
            errorHandler.handle(
                    new CompilationException(CompilationErrorLevel.WARNING, position,
                            "Function has no return statement.")
            );
        }

        return lines;
    }

    public List<AssemblyLine> setupStackFrame(Context context) {
        if (localSize < 0) throw new IndexOutOfBoundsException("Local stack size cannot be negative");
        if (localSize == 0) return new ArrayList<AssemblyLine>();

        ArrayList<AssemblyLine> lines = new ArrayList<AssemblyLine>();
        // Setup local variable stack space (if needed)
        if (localSize > 0) {
            // We are using a register as the "base point"
            // This will save the stack pointer location before we start adding to it with local variables
            context.getRegisterBank().use(BASE_STACK_POINTER_REGISTER);
            lines.add(BasicInstruction.push(BASE_STACK_POINTER_REGISTER));
            lines.add(BasicInstruction.set(BASE_STACK_POINTER_REGISTER, StackLocation.SP));

            // "Add" space for the variables. We are subtracting since the stack grows down
            lines.add(new BasicInstruction(BasicOpcode.SUB, StackLocation.SP, localSizeAsShort()));
        }

        return lines;
    }

    public List<AssemblyLine> exitStackFrame(Context context) {
        if (localSize < 0) throw new IndexOutOfBoundsException("Local stack size cannot be negative");
        if (localSize == 0) return new ArrayList<AssemblyLine>();

        ArrayList<AssemblyLine> lines = new ArrayList<AssemblyLine>();
        if (localSize > 0) {
            // "Subtract" space for the variables. We are adding since the stack grows down
            lines.add(new BasicInstruction(BasicOpcode.ADD, StackLocation.SP, localSizeAsShort()));

            // Restore the register we used for our base pointer back
            lines.add(new BasicInstruction(BasicOpcode.SET, StackLocation.SP, BASE_STACK_POINTER_REGISTER));
            lines.add(BasicInstruction.pop(BASE_STACK_POINTER_REGISTER));
            context.getRegisterBank().restore(BASE_STACK_POINTER_REGISTER);
        }

        // Return to caller
        lines.add(new BasicInstruction(BasicOpcode.SET, Register.PC, StackLocation.POP));

        return lines;
    }

    private Short localSizeAsShort() {
        return ((Integer) localSize).shortValue();
    }

    @Override
    public String toString() {
        return "Function Declaration |";
    }

    @Override
    public String getGlobalPrefix() {
        return GlobalType.FUNCTION.getGlobalPrefix();
    }

    @Override
    public String getQualifiedName() {
        return getGlobalPrefix() + identifier.getValue();
    }
}
