package com.jaxrtech.swift.entities.nodes.parameters;

import com.jaxrtech.swift.entities.nodes.IntegerLiteralNode;
import org.parboiled.trees.ImmutableTreeNode;
import org.parboiled.trees.TreeNode;

public class ParameterCallNode<T extends TreeNode<T>> extends ImmutableTreeNode<T> {
    // TODO: we are just dealing with word integers right now to keep things simple
    private IntegerLiteralNode value;

    public ParameterCallNode(IntegerLiteralNode value) {
        this.value = value;
    }

    public IntegerLiteralNode getValue() {
        return value;
    }
}
