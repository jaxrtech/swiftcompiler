package com.jaxrtech.swift.entities.nodes.parameters;

import org.parboiled.trees.ImmutableTreeNode;
import org.parboiled.trees.TreeNode;

import java.util.List;

public class ParameterCallSequenceNode<T extends TreeNode<T>> extends ImmutableTreeNode<T> {
    private List<ParameterCallNode> parameters;

    public ParameterCallSequenceNode(List<ParameterCallNode> parameters) {
        this.parameters = parameters;
    }

    public List<ParameterCallNode> getParameters() {
        return parameters;
    }
}
