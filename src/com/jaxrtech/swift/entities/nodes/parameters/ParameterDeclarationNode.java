package com.jaxrtech.swift.entities.nodes.parameters;

import com.jaxrtech.swift.entities.nodes.IdentifierNode;
import org.parboiled.trees.ImmutableTreeNode;
import org.parboiled.trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class ParameterDeclarationNode<T extends TreeNode<T>> extends ImmutableTreeNode<T> {
    private IdentifierNode name;
    private IdentifierNode type;

    public ParameterDeclarationNode(IdentifierNode name, IdentifierNode type) {
        this.name = new IdentifierNode("name", name.getValue());
        this.type = new IdentifierNode("type", type.getValue());
    }

    public IdentifierNode getName() {
        return name;
    }

    public IdentifierNode getType() {
        return type;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> getChildren() {
        ArrayList<T> list = new ArrayList<T>();
        list.add((T) name);
        list.add((T) type);

        return list;
    }

    @Override
    public String toString() {
        return "Parameter |";
    }
}
