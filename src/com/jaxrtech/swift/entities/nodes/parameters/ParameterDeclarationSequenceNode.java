package com.jaxrtech.swift.entities.nodes.parameters;

import org.parboiled.trees.ImmutableTreeNode;
import org.parboiled.trees.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class ParameterDeclarationSequenceNode<T extends TreeNode<T>> extends ImmutableTreeNode<T> {
    private List<ParameterDeclarationNode> parameters = new ArrayList<ParameterDeclarationNode>();

    public ParameterDeclarationSequenceNode(List<ParameterDeclarationNode> parameters) {
        this.parameters.addAll(parameters);
    }

    public List<ParameterDeclarationNode> getParameters() {
        return parameters;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> getChildren() {
        ArrayList<T> list = new ArrayList<T>();
        for (ParameterDeclarationNode node : parameters)
            list.add((T) node);

        return list;
    }

    @Override
    public String toString() {
        return "Parameter Sequence |";
    }
}
