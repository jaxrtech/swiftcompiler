package com.jaxrtech.swift.entities.operators;

public enum ArithmeticOperator {
    ADDITION("+"),
    SUBTRACTION("-"),
    MULTIPLICATION("*"),
    DIVISION("/"),
    MODULUS("%");

    private String operator;

    ArithmeticOperator(String operator) {
        this.operator = operator;
    }

    public String getOperator() {
        return operator;
    }

    public static ArithmeticOperator fromString(String op) {
        op = op.trim();
        if (op.equals(ADDITION.getOperator())) {
            return ADDITION;
        } else if (op.equals(SUBTRACTION.getOperator())) {
            return SUBTRACTION;
        } else if (op.equals(MULTIPLICATION.getOperator())) {
            return MULTIPLICATION;
        } else if (op.equals(DIVISION.getOperator())) {
            return DIVISION;
        } else if (op.equals(MODULUS.getOperator())) {
            return MODULUS;
        } else {
            throw new IllegalStateException("Invalid operator of '" + op + "'");
        }
    }
}
