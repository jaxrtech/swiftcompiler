package com.jaxrtech.swift.entities.operators;

public enum AssignmentOperator {
    BASIC_ASSIGNMENT("="),
    ADDITION_ASSIGNMENT("+="),
    SUBTRACTION_ASSIGNMENT("-="),
    MULTIPLICATION_ASSIGNMENT("*="),
    DIVISION_ASSIGNMENT("/="),
    MODULUS_ASSIGNMENT("%=");

    private String operator;

    AssignmentOperator(String operator) {
        this.operator = operator;
    }

    public String getOperator() {
        return operator;
    }

    public static AssignmentOperator fromString(String op) {
        op = op.trim();
        if (op.equals(BASIC_ASSIGNMENT.getOperator())) {
            return BASIC_ASSIGNMENT;
        } else if (op.equals(ADDITION_ASSIGNMENT.getOperator())) {
            return ADDITION_ASSIGNMENT;
        } else if (op.equals(SUBTRACTION_ASSIGNMENT.getOperator())) {
            return SUBTRACTION_ASSIGNMENT;
        } else if (op.equals(MULTIPLICATION_ASSIGNMENT.getOperator())) {
            return MULTIPLICATION_ASSIGNMENT;
        } else if (op.equals(DIVISION_ASSIGNMENT.getOperator())) {
            return DIVISION_ASSIGNMENT;
        } else if (op.equals(MODULUS_ASSIGNMENT.getOperator())) {
            return MODULUS_ASSIGNMENT;
        } else {
            throw new IllegalStateException("Invalid assignment operator of '" + op + "'");
        }
    }
}
