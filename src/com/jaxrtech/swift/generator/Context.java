package com.jaxrtech.swift.generator;

import com.jaxrtech.swift.Util;
import com.jaxrtech.swift.entities.nodes.ProgramNode;
import com.jaxrtech.swift.entities.nodes.VariableNode;
import com.jaxrtech.swift.entities.nodes.functions.FunctionDeclarationNode;
import com.jaxrtech.swift.generator.addresses.Addressable;
import com.jaxrtech.swift.generator.addresses.PointerAddress;
import com.jaxrtech.swift.generator.addresses.RegisterBank;
import com.jaxrtech.swift.generator.entities.LocalVariable;
import org.parboiled.trees.TreeNode;

import java.util.HashMap;
import java.util.Map;

public class Context implements Cloneable {
    private ProgramNode rootNode;
    private TreeNode parentNode;
    private TreeNode currentNode;
    private Map<String, FunctionDeclarationNode> globalFunctions = new HashMap<String, FunctionDeclarationNode>();
    private Map<String, VariableNode> globalVariables = new HashMap<String, VariableNode>();
    private Map<String, LocalVariable> localVariables = new HashMap<String, LocalVariable>();
    private RegisterBank registerBank;

    public Context(ProgramNode rootNode, RegisterBank registerBank) {
        this.rootNode = rootNode;
        this.parentNode = rootNode;
        this.currentNode = rootNode;
        this.registerBank = registerBank;
    }

    public static Context fromPrevious(TreeNode currentNode, Context previousContext) {
        try {
            Context context = (Context) previousContext.clone();
            context.parentNode = context.currentNode;
            context.currentNode = currentNode;
            return context;
        } catch (CloneNotSupportedException ex) {
            throw new IllegalStateException("Unable to clone Context");
        }
    }

    public GlobalType getTypeFromRelativeName(String name) {
        // Goes up in scope
        if (localVariablesContain(GlobalType.LOCAL_VARIABLE.getGlobalPrefix() + name)) {
            return GlobalType.LOCAL_VARIABLE;
        } else if (globalVariablesContain(GlobalType.GLOBAL_VARIABLE.getGlobalPrefix() + name)) {
            return GlobalType.GLOBAL_VARIABLE;
        } else if (globalFunctionsContain(GlobalType.FUNCTION.getGlobalPrefix() + name)) {
            return GlobalType.FUNCTION;
        } else {
            return GlobalType.INVALID;
        }
    }

    public String getQualifiedFromRelative(String name) {
        GlobalType type = getTypeFromRelativeName(name);
        if (type == GlobalType.INVALID)
            throw new IllegalStateException("The name '" + name + "' is invalid in the current context");
        return type.getGlobalPrefix() + name;
    }

    public TreeNode getFirstAncestorOfType(Class type) throws IllegalStateException {
        return Util.treeNodeGetFirstAncestorOfType(this.currentNode, type);
    }

    public boolean hasAncestorOfType(Class type) {
        return Util.treeNodeHasAncestorOfType(this.currentNode, type);
    }

    public ProgramNode getRootNode() {
        return rootNode;
    }

    public TreeNode getParentNode() {
        return parentNode;
    }

    public TreeNode getCurrentNode() {
        return currentNode;
    }

    public Map<String, FunctionDeclarationNode> getGlobalFunctions() {
        return globalFunctions;
    }

    public Addressable getAddressFromRelative(String relativeName) {
        String varQualifiedName = getQualifiedFromRelative(relativeName);
        switch (GlobalType.getTypeFromQualified(varQualifiedName)) {
            case GLOBAL_VARIABLE:
                return new PointerAddress(varQualifiedName);
            case LOCAL_VARIABLE:
                return getLocalVariable(varQualifiedName);
            // TODO: eventually get function pointers in here
            default:
                // TODO: have a consistent way of managing compiler errors
                System.err.println("Error: Either the reference called '" + relativeName + "' does not exist or is not a variable");
                throw new IllegalStateException("Invalid reference");
        }
    }

    public boolean globalFunctionsContain(String qualifiedName) {
        return globalFunctions.containsKey(qualifiedName);
    }

    public FunctionDeclarationNode getGlobalFunction(String qualifiedName) {
        return globalFunctions.get(qualifiedName);
    }

    public void setGlobalFunction(FunctionDeclarationNode function) {
        globalFunctions.put(function.getQualifiedName(), function);
    }

    public Map<String, VariableNode> getGlobalVariables() {
        return globalVariables;
    }

    public boolean globalVariablesContain(String qualifiedName) {
        return globalVariables.containsKey(qualifiedName);
    }

    public RegisterBank getRegisterBank() {
        return registerBank;
    }

    public VariableNode getGlobalVariable(String qualifiedName) {
        return globalVariables.get(qualifiedName);
    }

    public void setGlobalVariable(VariableNode variable) {
        globalVariables.put(variable.getQualifiedName(), variable);
    }

    public boolean localVariablesContain(String qualifiedName) {
        return localVariables.containsKey(qualifiedName);
    }

    public LocalVariable getLocalVariable(String qualifiedName) {
        return localVariables.get(qualifiedName);
    }

    public void setLocalVariable(LocalVariable variable) {
        localVariables.put(variable.getQualifiedName(), variable);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
