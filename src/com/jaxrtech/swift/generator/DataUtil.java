package com.jaxrtech.swift.generator;

import java.util.Formatter;

public final class DataUtil {
    public static String wordToString(short word) {
        StringBuilder sb = new StringBuilder();

        sb.append("0x");
        Formatter formatter = new Formatter(sb);
        formatter.format("%04x", word);

        return sb.toString();
    }
}
