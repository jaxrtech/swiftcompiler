package com.jaxrtech.swift.generator;

import com.jaxrtech.swift.compiler.CompilationErrorHandler;
import com.jaxrtech.swift.generator.entities.CallingConvention;
import com.jaxrtech.swift.generator.entities.FastCallConvention;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;
import com.jaxrtech.swift.entities.nodes.ProgramNode;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class Generator {
    private static final CallingConvention DEFAULT_CALLING_CONVENTION = new FastCallConvention();
    private ProgramNode root;

    public Generator(ProgramNode root) {
        this.root = root;
    }

    @SuppressWarnings({"unchecked"})
    public void generate(Writer writer, CompilationErrorHandler errorHandler) throws IOException {
        Context context = new Context(root, DEFAULT_CALLING_CONVENTION.getRegisterBank());
        List<AssemblyLine> generated = root.generate(context, errorHandler);

        for (AssemblyLine line : generated) {
            if (line == null) continue;
            writer.write(line.generate() + System.getProperty("line.separator"));
        }
        writer.close();
    }
}
