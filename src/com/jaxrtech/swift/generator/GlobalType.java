package com.jaxrtech.swift.generator;

public enum GlobalType {
    INVALID(""),
    LOCAL_VARIABLE("local_"),
    GLOBAL_VARIABLE("global_"),
    FUNCTION("func_");

    private String globalPrefix;

    private GlobalType(String globalPrefix) {
        this.globalPrefix = globalPrefix;
    }

    public String getGlobalPrefix() {
        return globalPrefix;
    }

    public static GlobalType getTypeFromQualified(String qualified) {
        if (qualified.startsWith(GLOBAL_VARIABLE.getGlobalPrefix())) {
            return GLOBAL_VARIABLE;
        } else if (qualified.startsWith(FUNCTION.getGlobalPrefix())) {
            return FUNCTION;
        } else if (qualified.startsWith(LOCAL_VARIABLE.getGlobalPrefix())) {
            return LOCAL_VARIABLE;
        } else {
            return INVALID;
        }
    }
}
