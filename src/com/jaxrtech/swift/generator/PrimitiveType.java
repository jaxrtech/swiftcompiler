package com.jaxrtech.swift.generator;

public enum PrimitiveType {
    SIGNED_INT("int", 1),
    UNSIGNED_INT("uint", 1),
    CHARACTER("char", 1);

    private String name;
    private int wordSize;

    private PrimitiveType(String name, int wordSize) {
        this.name = name;
        this.wordSize = wordSize;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public int getWordSize() {
        return wordSize;
    }

    public static PrimitiveType fromName(String name) {
        if (name.equals(UNSIGNED_INT.toString())) {
            return UNSIGNED_INT;
        } else if (name.equals(SIGNED_INT.toString())) {
            return SIGNED_INT;
        } else if (name.equals(CHARACTER.toString())) {
            return CHARACTER;
        } else {
            return null;
        }
    }
}
