package com.jaxrtech.swift.generator.addresses;

public interface Addressable {
    public String getAddress();
}
