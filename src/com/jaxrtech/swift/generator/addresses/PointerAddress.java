package com.jaxrtech.swift.generator.addresses;

public class PointerAddress implements Addressable {
    private String name;

    public PointerAddress(String name) {
        this.name = name;
    }

    @Override
    public String getAddress() {
        return "[" + name + "]";
    }
}
