package com.jaxrtech.swift.generator.addresses;

public class PushableRegister {
    private Register register;
    private boolean isPushed;

    public PushableRegister(Register register, boolean pushed) {
        this.register = register;
        isPushed = pushed;
    }

    public Register getRegister() {
        return register;
    }

    public boolean isPushed() {
        return isPushed;
    }
}
