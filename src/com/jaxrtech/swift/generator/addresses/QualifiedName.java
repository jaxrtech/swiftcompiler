package com.jaxrtech.swift.generator.addresses;

public interface QualifiedName {
    public String getGlobalPrefix();
    public String getQualifiedName();
}
