package com.jaxrtech.swift.generator.addresses;

public enum Register implements Addressable {
    A,
    B,
    C,
    X,
    Y,
    Z,
    I,
    J,
    SP,
    PC,
    EX;

    @Override
    public String getAddress() {
        return this.toString();
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
