package com.jaxrtech.swift.generator.addresses;

import com.jaxrtech.swift.generator.instructions.AssemblyLine;
import com.jaxrtech.swift.generator.instructions.AssemblyListingWithRegister;
import com.jaxrtech.swift.generator.instructions.BasicInstruction;

import java.util.*;

public class RegisterBank {
    private LinkedHashMap<Register, TypedRefCountedRegister> states = new LinkedHashMap<Register, TypedRefCountedRegister>();

    public RegisterBank(LinkedHashMap<Register, TypedRefCountedRegister> states) {
        this.states = states;
    }

    public TypedRefCountedRegister getRegisterState(Register register) {
        return states.get(register);
    }

    public AssemblyListingWithRegister autoManageNextRegister(RegisterType type, RegisterWrapper wrapper) {
        ArrayList<AssemblyLine> lines = new ArrayList<AssemblyLine>();
        Register register = getNextGeneralPurposeRegisterLowest();
        use(register);

        // Check to see if we need to push it before we use it
        boolean isPushed = states.get(register).getRefrenceCount() > 0;

        // Push it first if necessary
        if (isPushed) {
            lines.add(BasicInstruction.push(register));
        }

        // Add the code using the register
        lines.addAll(wrapper.generate(register));

        // Pop if necessary
        if (isPushed) {
            lines.add(BasicInstruction.pop(register));
        }

        restore(register);

        return new AssemblyListingWithRegister(lines, register);
    }

    /**
     * Gets the next general purpose register with the lowest reference count. This does <i>NOT</i> adjust the current
     * register states. This simply returns it and does not do anything to the states.
     * @return the register
     */
    @SuppressWarnings({"unchecked"})
    private Register getNextGeneralPurposeRegisterLowest() {
        LinkedHashMap<Register, TypedRefCountedRegister> registers = (LinkedHashMap<Register, TypedRefCountedRegister>) states.clone();

        // Remove the special registers from our copy since we are only dealing with general purpose registers
        // Note: the only reason we have to do this in this convoluted was it to remove the items safely without a
        //       ConcurrentModification exception.
        //       from http://stackoverflow.com/questions/223918/efficient-equivalent-for-removing-elements-while-iterating-the-collection
        Set<Map.Entry<Register, TypedRefCountedRegister>> set = registers.entrySet();
        for (Iterator<Map.Entry<Register, TypedRefCountedRegister>> iterator = set.iterator(); iterator.hasNext(); ) {
            Map.Entry<Register, TypedRefCountedRegister> entry = iterator.next();
            if (entry.getValue().getType() == RegisterType.SPECIAL_USE) iterator.remove();
        }

        // TODO: this comparison is all messed up
        // Get all registers and then sort them by their push count
        Comparator<TypedRefCountedRegister> lowestRefCountComparitor = new Comparator<TypedRefCountedRegister>() {
            @Override public int compare(TypedRefCountedRegister r1, TypedRefCountedRegister r2) {
                return ((Integer) r1.getRefrenceCount()).compareTo(r2.getRefrenceCount());
            }
        };

        SortedMap<TypedRefCountedRegister,Register> sortedRegisters = new TreeMap<TypedRefCountedRegister,Register>(lowestRefCountComparitor);
        // Copy in the register bank to be sorted
        for (Map.Entry<Register, TypedRefCountedRegister> entry : registers.entrySet()) {
            sortedRegisters.put(entry.getValue(), entry.getKey());
        }

        // Get the first one
        return sortedRegisters.get(sortedRegisters.firstKey());
    }

    /**
     * Gets the register in the register bank that matches a certain type and state
     * @param type the type of the register to get
     * @param state the state of the register to get
     * @return the first matched register or {@code null} if no register matches the arguments
     */
    private Register getNextRegister(RegisterType type, RegisterState state) {
        for (Map.Entry<Register, TypedRefCountedRegister> entry : states.entrySet()) {
            if (entry.getValue().getType() == type && entry.getValue().getState() == state) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * Increments the reference count of the register
     * @param register the register to use
     */
    public void use(Register register) {
        TypedRefCountedRegister state = states.get(register);
        state.incrementRefrenceCount();
        states.put(register, state);
    }

    /**
     * Decrements the reference count of the register
     * @param register the register to restore
     */
    public void restore(Register register) {
        TypedRefCountedRegister state = states.get(register);
        state.deincrementRefrenceCount();
        states.put(register, state);
    }
}
