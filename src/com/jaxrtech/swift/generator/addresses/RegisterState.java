package com.jaxrtech.swift.generator.addresses;

/**
 * Stores the current state of a register indicating whether it current use situation
 */
public enum RegisterState {
    /**
     * A register that is currently free and can be used immediately
     */
    FREE,
    /**
     * A register that is currently in use and needs to be saved for it can be used
     */
    USED,
    /**
     * A register that is currently saved on the stack
     */
    SAVED
}
