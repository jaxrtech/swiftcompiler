package com.jaxrtech.swift.generator.addresses;

public enum RegisterType {
    /**
     * A register that can only be used freely by the callee. The callee has full control over a register marked as this
     * causing the caller to push any registers marked as this before they are used.
     */
    CALLEE_USE,
    /**
     * A register that can only be used freely by the caller. The caller has full control over a register marked as this
     * causing the callee to push any register marked as this before they are used.
     */
    CALLER_USE,
    /**
     * A register that has a special used. These are not used as general purpose registers for computation.
     */
    SPECIAL_USE,
}
