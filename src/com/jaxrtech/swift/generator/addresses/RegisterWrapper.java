package com.jaxrtech.swift.generator.addresses;

import com.jaxrtech.swift.generator.instructions.AssemblyLine;

import java.util.List;

public interface RegisterWrapper {
    public List<AssemblyLine> generate(Register register);
}
