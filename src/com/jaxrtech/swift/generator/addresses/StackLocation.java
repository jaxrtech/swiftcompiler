package com.jaxrtech.swift.generator.addresses;

/* TODO: Might want to just move this to the Register enum instead
 * TODO: or the other (better) option is make this a class so that we can specify a stack offset
*/
public enum StackLocation implements Addressable {
    SP, PUSH, POP;

    @Override
    public String getAddress() {
        return this.toString().toLowerCase();
    }
}
