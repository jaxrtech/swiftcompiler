package com.jaxrtech.swift.generator.addresses;

public class TypedRefCountedRegister {
    private RegisterType type;
    private int refrenceCount;

    public TypedRefCountedRegister(RegisterType type) {
        this.type = type;
        this.refrenceCount = -1;
    }

    public TypedRefCountedRegister(RegisterType type, int referenceCount) {
        this.refrenceCount = referenceCount;
        this.type = type;
    }

    /**
     * @return the usage type for the register
     */
    public RegisterType getType() {
        return type;
    }

    /**
     * @return the current register state
     */
    public RegisterState getState() {
        if (refrenceCount < -1) {
            throw new IllegalStateException("Reference count cannot be below -1");
        } else if (refrenceCount == -1) {
            return RegisterState.FREE;
        } else if (refrenceCount == 0) {
            return RegisterState.USED;
        } else {
            return RegisterState.SAVED;
        }
    }

    /**
     * Returns the current reference count for the register.
     * The following values mean the following concerning the current state of the register:
     * <table>
     *     <thead>
     *         <td>Value</td>
     *         <td>Meaning</td>
     *     </thead>
     *     <tr>
     *         <td>-1</td>
     *         <td>Register is free and can be immediately used</td>
     *     </tr>
     *     <tr>
     *         <td>0</td>
     *         <td>Register is in use and must be pushed before it's used for another value</td>
     *     </tr>
     *     <tr>
     *         <td><i>n</i> > 0</td>
     *         <td>Register has been pushed <i>n</i> amount of times</td>
     *     </tr>
     * </table>
     * @return the reference count value for the register
     */
    public int getRefrenceCount() {
        return refrenceCount;
    }

    public void incrementRefrenceCount() {
        refrenceCount++;
    }

    public void deincrementRefrenceCount() {
        refrenceCount--;
    }
}
