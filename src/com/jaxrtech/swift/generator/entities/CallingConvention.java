package com.jaxrtech.swift.generator.entities;

import com.jaxrtech.swift.generator.addresses.RegisterBank;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;

import java.util.List;

public interface CallingConvention {
    public List<AssemblyLine> call(String qualifiedName, short[] parameters);
    public RegisterBank getRegisterBank();
}
