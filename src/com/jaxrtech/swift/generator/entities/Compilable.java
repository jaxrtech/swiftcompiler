package com.jaxrtech.swift.generator.entities;

import com.jaxrtech.swift.compiler.CompilationErrorHandler;
import com.jaxrtech.swift.generator.Context;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;

import java.util.List;

public interface Compilable {
    public List<AssemblyLine> generate(Context context, CompilationErrorHandler errorHandler);
}
