package com.jaxrtech.swift.generator.entities;

import org.parboiled.support.Position;
import org.parboiled.trees.ImmutableTreeNode;
import org.parboiled.trees.TreeNode;

public abstract class CompilableNode<T extends TreeNode<T>> extends ImmutableTreeNode<T> implements Compilable {
    protected Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
