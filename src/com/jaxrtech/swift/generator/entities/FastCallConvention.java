package com.jaxrtech.swift.generator.entities;

import com.jaxrtech.swift.generator.addresses.*;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;
import com.jaxrtech.swift.generator.instructions.BasicInstruction;
import com.jaxrtech.swift.generator.instructions.SpecialIntruction;
import com.jaxrtech.swift.generator.opcodes.BasicOpcode;
import com.jaxrtech.swift.generator.opcodes.SpecialOpcode;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

// TODO: going to need a way to manage registers (e.g. getting a free one, etc.) either in the calling convention or
// TODO: some other class (maybe in another class)
public class FastCallConvention implements CallingConvention {
    public FastCallConvention() {
    }

    @Override
    public List<AssemblyLine> call(String qualifiedName, short[] parameters) {
        List<AssemblyLine> lines = new ArrayList<AssemblyLine>();

        // Load first three parameters into registers A, B, and C
        for (int i = 0; i < parameters.length; i++) {
            short value = parameters[i];
            Addressable address = getParameterLocationAt(i);
            if (address != null) {
                lines.add(new BasicInstruction(
                        BasicOpcode.SET, address, value
                ));
            }
            // Only have 3 registers for parameters
            if (i == 2) break;
        }

        // Load the rest of the parameters backwards onto the stack
        if (parameters.length > 3) {
            for (int i = parameters.length; i > parameters.length - 3; i--) {
                short value = parameters[i];
                lines.add(new BasicInstruction(
                        BasicOpcode.SET, StackLocation.PUSH, value
                ));
            }
        }

        // Actually jump to the function
        lines.add(new SpecialIntruction(
                SpecialOpcode.JSR, qualifiedName
        ));

        return lines;
    }

    @Override
    public RegisterBank getRegisterBank() {
        // TODO: make a default set of registers to make this easier
        LinkedHashMap<Register, TypedRefCountedRegister> states = new LinkedHashMap<Register, TypedRefCountedRegister>();
        states.put(Register.A, new TypedRefCountedRegister(RegisterType.CALLEE_USE));
        states.put(Register.B, new TypedRefCountedRegister(RegisterType.CALLEE_USE));
        states.put(Register.C, new TypedRefCountedRegister(RegisterType.CALLEE_USE));

        states.put(Register.X, new TypedRefCountedRegister(RegisterType.CALLER_USE));
        states.put(Register.Y, new TypedRefCountedRegister(RegisterType.CALLER_USE));
        states.put(Register.Z, new TypedRefCountedRegister(RegisterType.CALLER_USE));
        states.put(Register.I, new TypedRefCountedRegister(RegisterType.CALLER_USE));
        states.put(Register.J, new TypedRefCountedRegister(RegisterType.CALLER_USE));

        states.put(Register.EX, new TypedRefCountedRegister(RegisterType.SPECIAL_USE));
        states.put(Register.PC, new TypedRefCountedRegister(RegisterType.SPECIAL_USE));
        states.put(Register.SP, new TypedRefCountedRegister(RegisterType.SPECIAL_USE));
        return new RegisterBank(states);
    }

    public Addressable getParameterLocationAt(int index) {
        switch (index) {
            case 0:
                return Register.A;
            case 1:
                return Register.B;
            case 2:
                return Register.C;
            default:
                return null;
        }
    }
}
