package com.jaxrtech.swift.generator.entities;

import com.jaxrtech.swift.generator.GlobalType;
import com.jaxrtech.swift.generator.addresses.QualifiedName;
import com.jaxrtech.swift.generator.addresses.Addressable;
import com.jaxrtech.swift.generator.addresses.PointerAddress;
import com.jaxrtech.swift.generator.instructions.AssemblyLine;
import com.jaxrtech.swift.generator.instructions.BasicInstruction;
import com.jaxrtech.swift.generator.opcodes.BasicOpcode;
import com.jaxrtech.swift.entities.nodes.VariableNode;
import com.jaxrtech.swift.entities.nodes.functions.FunctionDeclarationNode;

public class LocalVariable implements QualifiedName, Addressable {
    private VariableNode variable;
    private int stackStartWord;

    public LocalVariable(VariableNode variable, int stackStartWord) {
        this.variable = variable;
        this.stackStartWord = stackStartWord;
    }

    public VariableNode getVariable() {
        return variable;
    }

    public int getStackStartIndexWord() {
        return stackStartWord;
    }

    @Override
    public String getGlobalPrefix() {
        return GlobalType.LOCAL_VARIABLE.getGlobalPrefix();
    }

    @Override
    public String getQualifiedName() {
        return getGlobalPrefix() + variable.getName().getValue();
    }

    @Override
    public String getAddress() {
        return new PointerAddress(
                FunctionDeclarationNode.BASE_STACK_POINTER_REGISTER.toString() +
                        "-" +
                        (stackStartWord + 1)
        ).getAddress();
    }

    public AssemblyLine generateLocalVariableSet(short value) {
        return new BasicInstruction(
                BasicOpcode.SET,
                new PointerAddress(
                        FunctionDeclarationNode.BASE_STACK_POINTER_REGISTER.toString() +
                                "-" +
                                Integer.toString(getStackStartIndexWord() + 1)
                ),
                value
        );
    }

    public AssemblyLine generateLocalVariableSet(Addressable address) {
        return new BasicInstruction(
                BasicOpcode.SET,
                new PointerAddress(
                        FunctionDeclarationNode.BASE_STACK_POINTER_REGISTER.toString() +
                                "-" +
                                Integer.toString(getStackStartIndexWord() + 1)
                ),
                address
        );
    }
}
