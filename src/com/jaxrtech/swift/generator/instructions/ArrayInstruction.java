package com.jaxrtech.swift.generator.instructions;

import com.jaxrtech.swift.generator.DataUtil;
import com.jaxrtech.swift.generator.opcodes.ArrayOpcode;

public class ArrayInstruction implements AssemblyLine {
    private ArrayOpcode opcode;
    private short[] values;

    public ArrayInstruction(ArrayOpcode opcode, short[] values) {
        this.opcode = opcode;
        this.values = values;
    }

    @Override
    public String generate() {
        String op = opcode.toString() + " ";
        StringBuilder builder = new StringBuilder();
        builder.append(op);

        for (int i = 0; i < values.length; i++) {
            String hex = DataUtil.wordToString(values[i]);
            if (i > 0) hex = ", " + hex;
            builder.append(hex);
        }

        return builder.toString();
    }
}
