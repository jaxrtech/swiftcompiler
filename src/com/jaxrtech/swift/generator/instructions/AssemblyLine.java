package com.jaxrtech.swift.generator.instructions;

public interface AssemblyLine {
    public String generate();
}
