package com.jaxrtech.swift.generator.instructions;

import com.jaxrtech.swift.generator.addresses.Register;

import java.util.List;

public final class AssemblyListingWithRegister {
    private List<AssemblyLine> lines;
    private Register register;

    public AssemblyListingWithRegister(List<AssemblyLine> lines, Register register) {
        this.lines = lines;
        this.register = register;
    }

    public List<AssemblyLine> getLines() {
        return lines;
    }

    public Register getRegister() {
        return register;
    }
}
