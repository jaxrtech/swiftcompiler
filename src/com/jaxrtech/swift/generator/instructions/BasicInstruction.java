package com.jaxrtech.swift.generator.instructions;

import com.jaxrtech.swift.generator.addresses.Addressable;
import com.jaxrtech.swift.generator.addresses.StackLocation;
import com.jaxrtech.swift.generator.opcodes.BasicOpcode;

public class BasicInstruction implements AssemblyLine {
    private BasicOpcode opcode;
    private String a;
    private String b;

    public BasicInstruction(BasicOpcode opcode, Addressable a, Addressable b) {
        this.opcode = opcode;
        this.a = a.getAddress();
        this.b = b.getAddress();
    }

    public BasicInstruction(BasicOpcode opcode, Addressable a, short b) {
        this.opcode = opcode;
        this.a = a.getAddress();
        this.b = Short.toString(b);
    }

    public static AssemblyLine set(Addressable address1, Addressable address2) {
        return new BasicInstruction(BasicOpcode.SET, address1, address2);
    }

    public static AssemblyLine set(Addressable address, short value) {
        return new BasicInstruction(BasicOpcode.SET, address, value);
    }

    public static AssemblyLine push(Addressable addressable) {
        return new BasicInstruction(BasicOpcode.SET, StackLocation.PUSH, addressable);
    }

    /**
     * Pops the current value on the stack to the address by writing it to a line of assembly
     * @param address the address to save the value on the stack to
     */
    public static AssemblyLine pop(Addressable address) {
        return new BasicInstruction(BasicOpcode.SET, address, StackLocation.POP);
    }

    @Override
    public String generate() {
        return opcode.toString() + " " + a + ", " + b;
    }
}
