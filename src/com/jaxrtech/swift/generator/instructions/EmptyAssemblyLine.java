package com.jaxrtech.swift.generator.instructions;

import java.util.ArrayList;
import java.util.List;

public class EmptyAssemblyLine implements AssemblyLine {
    private static EmptyAssemblyLine instance = new EmptyAssemblyLine();
    private static List<AssemblyLine> instanceAsList = new ArrayList<AssemblyLine>(1) {{
        add(instance);
    }};

    public static EmptyAssemblyLine getInstance() {
        return instance;
    }

    public static List<AssemblyLine> getInstanceAsList() {
        return instanceAsList;
    }

    private EmptyAssemblyLine() {
    }

    @Override
    public String generate() {
        return String.format("%n");
    }
}
