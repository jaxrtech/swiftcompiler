package com.jaxrtech.swift.generator.instructions;

public class Label implements AssemblyLine {
    private String name;

    public Label(String name) {
        this.name = name;
    }

    @Override
    public String generate() {
        return ":" + name;
    }
}
