package com.jaxrtech.swift.generator.instructions;

import com.jaxrtech.swift.generator.opcodes.SpecialOpcode;

public class SpecialIntruction implements AssemblyLine {
    private SpecialOpcode opcode;
    private String a;

    public SpecialIntruction(SpecialOpcode opcode, String a) {
        this.opcode = opcode;
        this.a = a;
    }

    @Override
    public String generate() {
        return opcode.toString() + " " + a;
    }
}
