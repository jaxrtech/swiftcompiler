package com.jaxrtech.swift.generator.opcodes;

public enum ArrayOpcode {
    DAT;

    @Override
    public String toString() {
        return Util.formatOpcode(super.toString());
    }
}
