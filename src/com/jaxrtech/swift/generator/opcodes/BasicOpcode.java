package com.jaxrtech.swift.generator.opcodes;

import com.jaxrtech.swift.entities.operators.ArithmeticOperator;
import com.jaxrtech.swift.entities.operators.AssignmentOperator;

public enum BasicOpcode {
    SET, ADD, SUB, MUL, DIV, MOD;

    @Override
    public String toString() {
        return Util.formatOpcode(super.toString());
    }

    public static BasicOpcode fromArithmeticOperator(ArithmeticOperator operator) {
        switch (operator) {
            case ADDITION:
                return ADD;
            case SUBTRACTION:
                return SUB;
            case MULTIPLICATION:
                return MUL;
            case DIVISION:
                return DIV;
            case MODULUS:
                return MOD;
            default:
                return null;
        }
    }

    public static BasicOpcode fromAssignmentOperator(AssignmentOperator operator) {
        switch (operator) {
            case BASIC_ASSIGNMENT:
                return SET;
            case ADDITION_ASSIGNMENT:
                return ADD;
            case SUBTRACTION_ASSIGNMENT:
                return SUB;
            case MULTIPLICATION_ASSIGNMENT:
                return MUL;
            case DIVISION_ASSIGNMENT:
                return DIV;
            case MODULUS_ASSIGNMENT:
                return MOD;
            default:
                return null;
        }
    }
}
