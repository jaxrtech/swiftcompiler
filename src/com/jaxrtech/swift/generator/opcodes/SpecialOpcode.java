package com.jaxrtech.swift.generator.opcodes;

public enum SpecialOpcode {
    JSR;

    @Override
    public String toString() {
        return Util.formatOpcode(super.toString());
    }
}
